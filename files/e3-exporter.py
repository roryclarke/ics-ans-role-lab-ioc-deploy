import os
import re
import time

import click
import yaml
from git import Repo
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY, GaugeMetricFamily

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))


class GitCollector(object):
    def __init__(self, iocs):
        self.iocs = iocs

    def collect(self):
        dirty = GaugeMetricFamily(
            "e3_repository_dirty",
            "E3 IOC git repository has non commited local changes",
            labels=["ioc"],
        )
        diff = GaugeMetricFamily(
            "e3_repository_local_commits",
            "E3 IOC git repository has local committed changes",
            labels=["ioc"],
        )
        for ioc in self.iocs:
            ioc_dir = "/opt/iocs/{}".format(re.sub("[^A-Za-z0-9-_]", "_", ioc["name"]))
            if os.path.isdir(ioc_dir) and os.path.isdir(os.path.join(ioc_dir, ".git")):
                repo = Repo(ioc_dir)
                dirty.add_metric(
                    [ioc["name"]], int(repo.is_dirty(untracked_files=True))
                )

                version = ioc["version"] if "version" in ioc else "master"
                diff_value = int(
                    repo.git.ls_remote("origin", version).split("\t")[0]
                    != repo.head.commit.hexsha
                )
                diff.add_metric([ioc["name"]], diff_value)
        yield dirty
        yield diff


@click.command()
@click.option("--port", default=12110, help="TCP port the exporter will listen on")
@click.option(
    "--config",
    default=os.path.join(SCRIPT_DIR, "e3-exporter.yaml"),
    type=click.Path(exists=True),
    help="Configuration file",
)
def main(port, config):
    with open(config, "r") as config_file:
        try:
            iocs = yaml.safe_load(config_file)
        except yaml.YAMLError as error:
            print(error)
            return
    REGISTRY.register(GitCollector(iocs))
    start_http_server(port)
    while True:
        time.sleep(1)


if __name__ == "__main__":
    main()
