# ics-ans-role-lab-ioc-deploy

Ansible role to deploy IOCs in the ICS lab.

VirtualBox driver is default with working NFS Docker 2.0 used to develope the tests offline
mark-failed-deployment was removed as it used VB

  1. Added cell tests
  2. Added compiler test from common
  3. Dropped OS checking for compiler setup because this is redundant if playbooks are always run on CentOS Docker/VM
  4. Virtualbox is the default scenario, docker added as another scenario

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lab-ioc-deploy
```

## License

BSD 2-clause
