import ast
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


def test_second_ioc_is_deployed(host):
    assert host.service("ioc@test-nfs").is_running


def test_ioc_deployment_result(host):
    ioc_deployment_result = "/tmp/ioc_deployment_result"

    assert host.file(ioc_deployment_result).exists
    assert host.file(ioc_deployment_result).is_file

    ioc_deployment_result = ast.literal_eval(
        host.file(ioc_deployment_result).content_string
    )

    # ioc_deployment_result should have two entries; one for each IOC
    assert len(ioc_deployment_result.keys()) == 4

    # test-nfs should be successful
    test_nfs = ioc_deployment_result["test-nfs"]
    assert test_nfs["successful"]

    # test-cell-fail should not be successful
    test_cell_fail = ioc_deployment_result["test-cell-fail"]
    assert not test_cell_fail["successful"]
    assert test_cell_fail["ansible_failed_result"]

    # test-no-epics-version should not be successful
    test_no_epics_version = ioc_deployment_result["test-no-epics-version"]
    assert not test_no_epics_version["successful"]
    assert test_no_epics_version["ansible_failed_result"]
    assert (
        test_no_epics_version["ansible_failed_result"]["assertion"]
        == "ioc.epics_version is defined"
    )

    # test-no-require-version should not be successful
    test_no_require_version = ioc_deployment_result["test-no-require-version"]
    assert not test_no_require_version["successful"]
    assert test_no_require_version["ansible_failed_result"]
    assert (
        test_no_require_version["ansible_failed_result"]["assertion"]
        == "ioc.require_version is defined"
    )
