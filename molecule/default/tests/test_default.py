import os

import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")

NOT_DEPLOYED_IOCS = ["do-not-deploy-me"]
IOCS = [
    "test-nfs",
    "test-conda",
    "test-realtime",
    "test-manual-start",
    "patch-cell-ioc",
    "test_packages_nfs_ioc",
]
STARTED_IOCS = [
    "test-nfs",
    "test-conda",
    "test-realtime",
    "patch-cell-ioc",
    "test_packages_nfs_ioc",
]
ENABLED_IOCS = [
    "test-nfs",
    "test-conda",
    "test-realtime",
    "patch-cell-ioc",
    "test_packages_nfs_ioc",
]

SET_E3_ENV_SH = "/epics/base-7.0.4/require/3.3.0/bin/setE3Env.bash"
CAGET = "/epics/base-7.0.4/bin/linux-x86_64/caget"
CAPUT = "/epics/base-7.0.4/bin/linux-x86_64/caput"


def host_caget(host, pv):
    return host.run(f"{CAGET} -w 60 -t {pv}")
    # return host.run(f'source {SET_E3_ENV_SH} > /dev/null && caget -w 60 -t {pv}')


def host_caput(host, pv, val):
    return host.run(f"{CAPUT} -w 60 {pv} {val}")


@pytest.mark.parametrize("iocname", NOT_DEPLOYED_IOCS)
def test_ioc_was_not_deployed(host, iocname):
    opt_iocs = "/opt/iocs"
    assert not host.file(os.path.join(opt_iocs, iocname)).exists


@pytest.mark.parametrize("iocname", IOCS)
def test_ioc_was_deployed(host, iocname):
    opt_iocs = "/opt/iocs"
    assert host.file(os.path.join(opt_iocs, iocname)).exists
    assert host.file(os.path.join(opt_iocs, iocname)).is_directory


def test_gcc(host):
    # Test gcc installed
    gcc = host.package("gcc")
    assert gcc.is_installed


def test_installed_package(host):
    image_magick = host.package("ImageMagick-devel")
    assert image_magick.is_installed


def test_cell_install(host):
    # Test that cell directory installed
    cell_log = host.file("/var/log/procServ/out-test-nfs.log")
    cmd = host.run(f"source {SET_E3_ENV_SH}; caget -w 60 -t test-nfs:testmodule_VER")
    assert cell_log.contains(
        "Module testmodule version master found in /opt/iocs/test-nfs/cell/cellMods/testmodule/master/"
    )
    assert cell_log.contains(
        "Loading library /opt/iocs/test-nfs/cell/cellMods/testmodule/master/lib/linux-x86_64/libtestmodule.so"
    )
    assert cmd.succeeded


def test_cell_patch(host):
    pv = "patch-cell-ioc:PatchStatus"
    cmd = host_caget(host, pv)
    assert cmd.succeeded
    assert "PATCHED" in cmd.stdout


def test_mlockall(host):
    pv = "IPC:LOCK:cap_ipc_lock_test"
    cmd = host_caget(host, f"{pv}")
    assert cmd.succeeded
    assert "42" in cmd.stdout

    cmd = host_caput(host, f"{pv}.PROC", 1)
    assert cmd.succeeded

    cmd = host_caget(host, f"{pv}")
    assert cmd.succeeded
    assert "0" in cmd.stdout

    cmd = host_caget(host, f"{pv}.E")
    assert cmd.succeeded
    assert "0" in cmd.stdout


def test_realtime(host):
    iocsh_bash = host.process.filter(comm="iocsh.bash")
    pid = 0
    for proc in iocsh_bash:
        args = proc["args"]
        if "test-realtime" in args:
            pid = proc.pid
            assert "--realtime" in args
            break

    assert pid

    softIoc = host.process.filter(comm="softIoc")
    softIoc.extend(host.process.filter(comm="softIocPVA"))
    for proc in softIoc:
        args = proc["args"]
        if f"_iocsh_-PID-{pid}" in args:
            # Scheduling policy; FF -- SCHED_FIFO,  RR - SCHED_RR
            assert proc.policy in ["FF", "RR"]
            # Real-time scheduling priority, a number in the range 1 to 99 for processes scheduled under a real-time policy, or 0, for non-real-time processes
            assert proc.rtprio > 0
            # From man 5 proc:
            # For processes running a real-time scheduling policy this is the negated scheduling priority, minus one; that is, a number in the range -2 to -100, corresponding to real-time priorities 1 to 99.
            # For processes running under a non-real-time scheduling policy, this is the raw nice value as represented in the kernel. The kernel stores nice values as numbers in the range 0 (high) to 39 (low), corresponding to the user-visible nice range of -20 to 19.
            # BUT: testinfra's Process uses `ps` to get the priority information, so priority above 39 is realtime prio
            assert proc.pri > 39
        else:
            assert proc.policy == "TS"
            assert proc.pri < 40
            assert proc.rtprio == "-"


def check_permissions(fl, is_dir=False):
    assert fl.exists
    if is_dir:
        assert fl.is_directory
        assert fl.mode == 0o0755
    else:
        assert fl.is_file
        assert fl.mode == 0o0644
    assert fl.user == "iocuser"
    assert fl.group == "iocgroup"


def test_procserv(host):
    procserv = host.package("procServ")
    assert procserv.is_installed

    procserv = host.file("/var/log/procServ")
    check_permissions(procserv, True)
    for f in procserv.listdir():
        check_permissions(host.file(os.path.join("/var/log/procServ", f)))


def test_conserver_running_and_enabled(host):
    conserver = host.service("conserver")
    assert conserver.is_running
    assert conserver.is_enabled


def test_iocuser_and_iocgroup_exist(host):
    iocuser = host.user("iocuser")
    iocgroup = host.group("iocgroup")
    assert iocuser.exists
    assert iocgroup.exists
    assert iocgroup.gid in iocuser.gids


def test_iocuser_in_realtime_group(host):
    realtime = host.group("realtime")
    assert realtime.exists
    assert realtime.gid in host.user("iocuser").gids


@pytest.mark.parametrize("iocname", IOCS)
def test_ioc_deployment_in_progress_is_removed(host, iocname):
    opt_iocs = "/opt/iocs"
    assert host.file(os.path.join(opt_iocs, iocname)).exists
    assert host.file(os.path.join(opt_iocs, iocname)).is_directory

    iocname_found = False
    for ioc in host.file(opt_iocs).listdir():
        if iocname == ioc:
            iocname_found = True
        dip = os.path.join(opt_iocs, ioc, ".deployment", ".deployment-in-progress")
        assert not host.file(dip).exists
    assert iocname_found


def test_e3_prometheus_exporter_running(host):
    exporter = host.service("e3-exporter")
    assert exporter.is_running
    assert exporter.is_enabled


@pytest.mark.parametrize("iocname", IOCS)
def test_ioc_service_started(host, iocname):
    ioc_service = host.service(f"ioc@{iocname}")
    assert ioc_service.is_running == (iocname in STARTED_IOCS)


@pytest.mark.parametrize("iocname", ENABLED_IOCS)
def test_ioc_service_enabled(host, iocname):
    ioc_service = host.service(f"ioc@{iocname}")
    assert ioc_service.is_enabled == (iocname in ENABLED_IOCS)


@pytest.mark.parametrize("iocname", IOCS)
def test_ioc_console_configured(host, iocname):
    cmd = host.run(f"console -I {iocname}")
    assert cmd.succeeded
    console_info = cmd.stdout
    assert f"{iocname}:" in console_info
    assert f"/run/ioc@{iocname}/control" in console_info
    # no longer testing if the console is `up`; conserver only brings consoles up when someone connects
    assert f"/var/log/conserver/{iocname}.log" in console_info


@pytest.mark.parametrize("iocname", STARTED_IOCS)
def test_ioc_pv_reachable(host, iocname):
    cmd = host_caget(host, f"{iocname}:BaseVersion")
    assert cmd.succeeded


def test_epics_version_pv(host):
    cmd = host_caget(host, "test-nfs:BaseVersion")
    assert cmd.succeeded
    assert "7.0.4" in cmd.stdout


def test_require_version_pv(host):
    cmd = host_caget(host, "test-nfs:require_VER")
    assert cmd.succeeded
    assert "3.3.0" in cmd.stdout


ioc_e3_versions = [
    ("test-nfs", "/epics/base-7.0.4/require/3.3.0"),
    ("test-manual-start", "/epics/base-7.0.5/require/3.4.0"),
]


@pytest.mark.parametrize("ioc_e3_versions", ioc_e3_versions)
def test_e3_version_paths_in_startup_script(host, ioc_e3_versions):
    iocname, e3_version = ioc_e3_versions
    start_ioc = f"/opt/iocs/{iocname}/.deployment/start_ioc.sh"
    content = host.file(start_ioc).content_string
    assert e3_version in content
