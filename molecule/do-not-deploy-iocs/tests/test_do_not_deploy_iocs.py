import os

import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")

IOCS = ["test-nfs", "test-conda", "test-manual-start"]
NOT_DEPLOYED_IOCS = IOCS


@pytest.mark.parametrize("iocname", NOT_DEPLOYED_IOCS)
def test_ioc_was_not_deployed(host, iocname):
    opt_iocs = "/opt/iocs"
    assert not host.file(os.path.join(opt_iocs, iocname)).exists


@pytest.mark.parametrize("iocname", IOCS)
def test_ioc_console_configured(host, iocname):
    cmd = host.run(f"console -I {iocname}")
    assert cmd.succeeded
    console_info = cmd.stdout
    assert f"{iocname}:" in console_info
    assert f"/run/ioc@{iocname}/control" in console_info
    # no longer testing if the console is `down`; conserver only brings consoles up when someone connects
    assert f"/var/log/conserver/{iocname}.log" in console_info
