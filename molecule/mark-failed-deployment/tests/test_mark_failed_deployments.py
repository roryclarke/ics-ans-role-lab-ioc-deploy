import ast
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


def test_ioc_deployment_in_progress(host):
    opt_iocs = "/opt/iocs"
    for ioc in host.file(opt_iocs).listdir():
        dip = os.path.join(opt_iocs, ioc, ".deployment", ".deployment-in-progress")
        assert host.file(dip).exists


def test_failed_at_the_same_task(host):
    first_ioc = "/tmp/ioc_failure"
    second_ioc = "/tmp/second_ioc_failure"

    assert host.file(first_ioc).exists
    assert host.file(first_ioc).is_file

    assert host.file(second_ioc).exists
    assert host.file(second_ioc).is_file

    fd = ast.literal_eval(host.file(first_ioc).content_string)
    assert list(fd.keys()) == ["test:cell-fail"]
    fd = fd["test:cell-fail"]

    assert not fd["successful"]
    fd = fd["ansible_failed_result"]

    sd = ast.literal_eval(host.file(second_ioc).content_string)
    assert list(sd.keys()) == ["test:cell-fail"]
    sd = sd["test:cell-fail"]

    assert not sd["successful"]
    sd = sd["ansible_failed_result"]

    assert fd["cmd"] == sd["cmd"]
    assert fd["stderr"] == sd["stderr"]
    assert fd["stdout"] == sd["stdout"]
